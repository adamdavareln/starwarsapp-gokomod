import React from 'react';
import { SafeAreaView } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Navigations from './src/navigations';
import { store } from './src/store';
import { Provider } from 'react-redux';

export default App = () => {
  return(
    <Provider store={store}>
      <SafeAreaView style={{flex:1, backgroundColor:'white'}}>
        <NavigationContainer>
          <Navigations/>
        </NavigationContainer>
      </SafeAreaView>
    </Provider>
  )
}