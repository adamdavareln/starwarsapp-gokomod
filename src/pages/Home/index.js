
import React, { useEffect, useState } from 'react';
import { View, ScrollView, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import MovieCard from '../../components/Card/MovieCard';
import SearchBar from '../../components/SearchBar';
import { getMovies } from '../../store/movie/actions';
import StarWarsBlackLogo from './../../assets/images/img-starwars-black.svg'
 
export default Home = () => {

    const [ loading, setLoading ] = useState(true)

    const dispatch = useDispatch()
    const {
        movies,
    } = useSelector(state => state.movie)

    useEffect(() => {
        getMovieData()
    },[])

    const getMovieData = async () => {
        setLoading(true)
        await dispatch(getMovies())
        setLoading(false)
    }

    return(
        <View style={{backgroundColor:'white',flex:1,}}>
            
            <ScrollView >
                <View
                    style={{
                        padding:24,
                        backgroundColor:'white',
                    }}
                >
                    <View style={{ alignItems:'center', }} >
                        <StarWarsBlackLogo/>
                    </View>
                    <SearchBar/>
                    {
                        loading ? 
                        <ActivityIndicator/> :
                        movies.map((movie,index) => {
                            return(
                                <MovieCard key={index} data={movie}/>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    )
}