import React, { useEffect, useState } from 'react';
import { View, Image, Text, ActivityIndicator, ScrollView } from 'react-native'
import FilmCard from '../../components/Card/FilmCard';
import Header from '../../components/Header';
import VehicleCard from '../../components/Card/VehicleCard';
import api from '../../services/api';
import { toPascalCase } from '../../services/helper';
import StarWarsWhiteLogo from './../../assets/images/img-starwars-white.png'

const CastRowData = ({label, value}) => {
  return(
    <View
      style={{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        marginBottom:12,
      }}
    >
      <Text style={{color:"#999"}}>{label}</Text>
      <Text>{value}</Text>
    </View>
  )
}

export default CastProfile = ({
  route
}) => {

  const [ castProfileData, setCastProfileData ] = useState()
  const [ loading, setLoading ] = useState(true)

  const {
    url,
  } = route.params

  useEffect(() => {
    getCastProfileData()
  }, [])

  const getCastProfileData = async () => {
    setLoading(true)
    const res = await api.get(url)
    if(res){
      setCastProfileData(res)
    }
    setLoading(false)
  }
  
  return(
    <View
      style={{flex:1, backgroundColor:'white'}}
    >
      <Header title={castProfileData?.name}/>
      <ScrollView>
        {
          loading ?
          <ActivityIndicator style={{marginTop:32,}}/> :
          <>
            <View
                style={{
                backgroundColor:'white',
                }}
            >
              <View style={{padding:24,}}>
                <Text style={{fontSize:20, marginBottom:20}}>Profile</Text>
                <View
                  style={{
                    height:120,
                    backgroundColor:'#25282b',
                    alignItems:'center',
                    justifyContent:'center',
                    marginBottom:16,
                  }}
                >
                  <Image 
                    source={StarWarsWhiteLogo}
                    resizeMode="contain"
                    style={{
                      width:'25%',
                    }}
                  />
                </View>
                <CastRowData label={"Name"} value={castProfileData?.name} />
                <CastRowData label={"Height"} value={castProfileData?.height + " cm"} />
                <CastRowData label={"Birth Year"} value={castProfileData?.name} />
                <CastRowData label={"Gender"} value={toPascalCase(castProfileData?.gender)} />
              </View>
              <View>
                <Text style={{fontSize:20, marginBottom:20, marginLeft:24,}}>Films</Text>
                <ScrollView
                  contentContainerStyle={{
                    paddingLeft:24,
                    paddingRight:18,
                    paddingBottom:32,
                  }}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                >
                  {
                    castProfileData?.films.map((film, i) => {
                      return(
                        <FilmCard key={i} url={film} />
                      )
                    })
                  }
                </ScrollView>
              </View>
            </View>
            {
              castProfileData?.vehicles.length > 0 &&
              <View style={{paddingHorizontal:24}}>
                <Text style={{fontSize:20, marginBottom:20}}>Vehicles</Text>
                {
                  castProfileData?.vehicles.map((vehicle,i) => {
                    return(
                      <VehicleCard key={i} url={vehicle}/>
                    )
                  })
                }
              </View>
            }
            {
              castProfileData?.starships.length > 0 &&
              <View style={{paddingHorizontal:24}}>
                <Text style={{fontSize:20, marginBottom:20}}>Star</Text>
                {
                  castProfileData?.starships.map((starship,i) => {
                    return(
                      <VehicleCard key={i} url={starship}/>
                    )
                  })
                }
              </View>
            }
          </>
        }
      </ScrollView>
    </View>
  )
}