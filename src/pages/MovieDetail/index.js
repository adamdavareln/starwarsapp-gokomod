/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text, ScrollView } from 'react-native'
import CastCard from '../../components/Card/CastCard';
import Header from '../../components/Header';
 
export default MovieDetail = ({
  route
}) => {

  const {
    movieDetail
  } = route.params
  
  return(
    <View
      style={{flex:1, backgroundColor:'white'}}
    >
      <Header title={movieDetail.title}/>
      <ScrollView>
        <View
            style={{
            padding:24,
            backgroundColor:'white',
            }}
        >
          <Text style={{fontSize:20, marginBottom:20}}>Cast</Text>
          {
            movieDetail.characters.map((char, i) => {
              return(
                <CastCard key={i} url={char}/>
              )
            })
          }
        </View>
      </ScrollView>
    </View>
  )
}