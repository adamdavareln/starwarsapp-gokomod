import { Platform } from "react-native"

export const SET_MOVIES = 'src/store/transaction/SET_MOVIES'
export const IS_IOS_PLATFORM = Platform.OS == "ios" ? true : false