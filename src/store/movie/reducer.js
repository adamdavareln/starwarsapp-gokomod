import * as constants from './constants'

const INITIAL_STATE = {
	movies:[],
}

const moviesReducer = (state = INITIAL_STATE, action) => {
	switch(action.type) {
		case constants.SET_MOVIES:
			return Object.assign({}, state, {
				movies:action.payload,
			})
		default: return state
	}
}

export default moviesReducer