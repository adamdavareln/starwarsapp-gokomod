import * as constants from "./constants";
import api from "../../services/api";
import { setDataNotFoundModalMessage, setDataNotFoundModalVisible } from "../page/actions";
/** Set */

export function setMovies(data) {
  return { type: constants.SET_MOVIES, payload: data };
}

/** Function */

export function getMovies() {
  return async dispatch => {
    try {
      const response = await api.get(`films`);
      if(response){
        dispatch(setMovies(response.results))
        return true
      } else {
        return false
      }
    } catch (error){
      return false
    }
  }
}
