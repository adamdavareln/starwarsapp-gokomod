import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import movieReducer from "./movie/reducer";

const rootReducer = {
  movie:movieReducer,
}

const reducer = combineReducers(rootReducer);

export const store = createStore(reducer, applyMiddleware(thunkMiddleware));
