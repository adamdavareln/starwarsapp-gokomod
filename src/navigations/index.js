import React from 'react'
import { createNativeStackNavigator } from "@react-navigation/native-stack"; 
import Home from "../pages/Home";
import MovieDetail from '../pages/MovieDetail';
import CastProfile from '../pages/CastProfile';

const Stack = createNativeStackNavigator()

const navigationOptions = {
    headerShown: false,
    gestureEnabled: false,
}

export default () => {
    
    return(
        <Stack.Navigator
            screenOptions={navigationOptions}
        >
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="MovieDetail" component={MovieDetail} />
            <Stack.Screen name="CastProfile" component={CastProfile} />
        </Stack.Navigator>
    )
}