import { useNavigation } from '@react-navigation/native';
import React from 'react';
 import { View, Text, TouchableOpacity } from 'react-native'
import { IS_IOS_PLATFORM } from '../../store/movie/constants';
import ChevronLeftIcon from './../../assets/images/chevron-left.svg'

export default Header = ({title}) => {

    const navigation = useNavigation()

    const shadowStyle = IS_IOS_PLATFORM ? {
      shadowColor:'#eee',
      shadowRadius:4,
      shadowOpacity:1,
      shadowOffset: { height: 2, },
    } : {
      elevation:4,
    }

    return (
      <View
        style={{
          height:48,
          backgroundColor:'white',
          zIndex:2,
          alignItems:'center',
          paddingHorizontal:16,
          flexDirection:'row',
          ...shadowStyle,
        }}
      >
        <TouchableOpacity
          onPress={() => {
            navigation.goBack()
          }}
        >
          <ChevronLeftIcon stroke={'black'} style={{marginRight:16,}}/>
        </TouchableOpacity>
        <Text style={{fontSize:16}}>{title}</Text>
      </View>
    )
  }