import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { IS_IOS_PLATFORM } from '../../../store/movie/constants';
import StarWarsYellowLogo from './../../../assets/images/img-starwars-yellow.png'
 
const shadowStyle = IS_IOS_PLATFORM ? {
  shadowColor:'#eee',
  shadowOffset: { height: 10 },
  shadowOpacity:1,
  shadowRadius:4,
} : {
  elevation:4,
}

export default MovieCard = ({data,}) => {
    const navigation = useNavigation()

    useEffect(()=> {

    },[])

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={() => {
            navigation.navigate("MovieDetail", {movieDetail:data})
        }}
        style={{
          borderRadius:8,
          backgroundColor:'white',
          marginBottom:24,
          zIndex:999,  
          ...shadowStyle,
        }}
      >
        <View
          style={{
            borderTopLeftRadius:8,
            borderTopRightRadius:8,
            height:164,
            backgroundColor:'#25282b',
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Image
            source={StarWarsYellowLogo}
            resizeMode="contain"
            style={{
              alignSelf:'center',
              width:'35%',
            }}
          />
        </View>
        <View
          style={{
            paddingHorizontal:16,
            paddingVertical:8,
          }}
        >
          <Text>{data.title}</Text>
        </View>
      </TouchableOpacity>
    )
  }