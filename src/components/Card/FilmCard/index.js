import React, { useEffect, useState } from 'react';
 import { View, Image, Text} from 'react-native'
import api from '../../../services/api';
import { IS_IOS_PLATFORM } from '../../../store/movie/constants';
import StarWarsYellowLogo from './../../../assets/images/img-starwars-yellow.png'

const shadowStyle = IS_IOS_PLATFORM ? {
  shadowColor:'#eee',
  shadowRadius:4,
  shadowOffset:{height:4},
  shadowOpacity:1,
} : {
  elevation:4,
}

export default FilmCard = ({url}) => {

  const [ filmData, setFilmData ] = useState()

  useEffect(()=>{
    getFilmData()
  },[])

  const getFilmData = async () => {
    const res = await api.get(url)
    if(res){
      setFilmData(res)
    }
  }

  return(
    <View
      style={{
        backgroundColor:'white',
        borderRadius:8,
        marginRight:12,
        width:124,
        ...shadowStyle,
      }}
    >
      <View
        style={{
          borderTopLeftRadius:8,
          borderTopRightRadius:8,
          height:124,
          backgroundColor:'#25282b',
          alignItems:'center',
          justifyContent:'center',
        }}                
      >
        <Image 
          source={StarWarsYellowLogo}
          resizeMode="contain"
          style={{
            width:'60%',
          }}
        />
      </View>
      <View
        style={{
          padding:8,
          flex:1,
        }}
      >
        <Text style={{flex:1,}} numberOfLines={2} ellipsizeMode="tail">{filmData?.title}</Text>
      </View>
    </View>
  )
}