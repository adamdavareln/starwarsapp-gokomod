import React, { useEffect, useState } from 'react';
 import { View, Image, Text, TouchableOpacity} from 'react-native'
import api from '../../../services/api';
import { IS_IOS_PLATFORM } from '../../../store/movie/constants';
import StarWarsWhiteLogo from './../../../assets/images/img-starwars-white.png'

const shadowStyle = IS_IOS_PLATFORM ? {
  shadowColor:'#eee',
  shadowRadius:4,
  shadowOpacity:1,
  shadowOffset: { height: 4 },
} : {
  elevation:4,
}

export default VehicleCard = ({url}) => {

  const [data, setData] = useState() 

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    const res = await api.get(url)
    if(res){
      setData(res)
    }
  }

  return(
    <TouchableOpacity
      style={{
        padding:8,
        borderRadius:8,
        elevation:16,
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:16,
        ...shadowStyle,
      }}
    >
      <View
        style={{
          backgroundColor:'#25282b',
          borderRadius:4,
          width:72,
          height:72,
          alignItems:'center',
          justifyContent:'center',
          marginRight:12,
        }}
      >
        <Image
          source={StarWarsWhiteLogo}
          resizeMode="contain"
          style={{
            width:52,
            height:52,
          }}
        />
      </View>
      <View>
        <Text
          style={{
            fontSize:14,
            textTransform:'capitalize'
          }}
        >{data?.name}</Text>
        <Text
          style={{
            fontSize:12,
            marginTop:8,
            textTransform:'capitalize'
          }}
        >{data?.model}</Text>
        <Text
          style={{
            fontSize:12,
            color:'#999',
            marginTop:8,
            textTransform:'capitalize'
          }}
        >{data?.manufacturer}</Text>
      </View>
    </TouchableOpacity>
  )
}