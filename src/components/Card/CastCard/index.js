import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
 import { View, Image, Text, TouchableOpacity} from 'react-native'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import api from '../../../services/api';
import { toPascalCase } from '../../../services/helper';
import { IS_IOS_PLATFORM } from '../../../store/movie/constants';
import StarWarsWhiteLogo from './../../../assets/images/img-starwars-white.png'

const shadowStyle = IS_IOS_PLATFORM ? {
  shadowColor:'#eee',
  shadowRadius:4,
  shadowOpacity:1,
  shadowOffset: { height: 4, },
} : {
  elevation:4,
}

export default CastCard = ({url}) => {

  const navigation = useNavigation()

  const [ peopleData, setPeopleData ] = useState()
  const [ loading, setLoading ] = useState(true)

  useEffect(() => {
    getPeopleData()
  },[])

  const getPeopleData = async () => {
    setLoading(true)
    const res = await api.get(url)
    if(res){
      setPeopleData(res)
    }
    setLoading(false)
  }

  return(
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => navigation.navigate('CastProfile', {url})}
      style={{
        padding:8,
        borderRadius:8,
        elevation:16,
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:16,
        ...shadowStyle,
      }}
    >
      <View
        style={{
          backgroundColor:'#25282b',
          borderRadius:4,
          width:52,
          height:52,
          alignItems:'center',
          justifyContent:'center',
          marginRight:12,
        }}
      >
        <Image
          source={StarWarsWhiteLogo}
          resizeMode="contain"
          style={{
            width:38,
            height:38,
          }}
        />
      </View>
      {
        loading ?
        <SkeletonPlaceholder>
          <SkeletonPlaceholder.Item width={80} height={16} />
          <SkeletonPlaceholder.Item marginTop={8} width={124} height={16} />
        </SkeletonPlaceholder>
        :
        <View>
          <Text
            style={{
              fontSize:16,
            }}
          >{peopleData?.name}</Text>
          <Text
            style={{
              fontSize:14,
              color:'#999',
              marginTop:8
            }}
          >{peopleData?.gender == "female" || peopleData?.gender == "male" ? toPascalCase(peopleData?.gender) : peopleData?.gender.toUpperCase()}, {peopleData?.height} cm</Text>
        </View>
      }
    </TouchableOpacity>
  )
}