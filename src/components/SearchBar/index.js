import React from 'react';
 import { Text, TouchableOpacity } from 'react-native'
import SearchIcon from '../../assets/images/search.svg'

export default SearchBar = () => {
    return (
      <TouchableOpacity
       style={{
         backgroundColor:'#fafafa',
         height:40,
         borderRadius:20,
         paddingHorizontal:16,
         alignItems:'center',
         flexDirection:'row',
         marginVertical:32,
       }}
     >
       <SearchIcon stroke={"#aaa"} width={16} height={16}/>
       <Text
         style={{
           color:'#aaa',
           marginLeft:12,
         }}
       >Search Star Wars Films</Text>
     </TouchableOpacity>
    )
  }