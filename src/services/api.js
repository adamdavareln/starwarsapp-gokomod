import axios from "axios";
import querystring from "qs";
import constants from "../constants";

const { BASE_URL } = constants;

const api = axios.create({
  baseURL: BASE_URL,
  timeout: 20000,
  headers: {
    Accept: "application/json",
    'Access-Control-Allow-Origin': '*',
  },
  paramsSerializer: (params) => querystring.stringify(params),
});

export default {
  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} json
   * @param {Object} form
   */
  put: async (url, form = {}, json = {}) => {
    api.defaults.headers.common["Content-Type"] = json
      ? "application/json"
      : "application/x-www-form-urlencoded";
    const data = querystring.stringify(form) || json;
    return api
      .put(url, data, {
        params: querystring.stringify(form),
        baseURL: BASE_URL,
      })
      .then(async (response) => Promise.resolve(response))
      .catch((err) => Promise.reject(err));
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} param query params
   */
  get: async (url, customConfig = {}, params = {}) => {
    return api
      .get(url, {
        baseURL: BASE_URL,
        params,
        ...customConfig,
      })
      .then(async (response) => {
          return Promise.resolve(response.data)
      })
      .catch((err) => Promise.reject(err));
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} json
   * @param {Object} form
   * @param {Object} reqConfig  custom config for request
   */
  post: async (url, form = {}, json = {}, reqConfig = {}) => {
    api.defaults.headers["Content-Type"] = !form
      ? "application/json"
      : "application/x-www-form-urlencoded";
    const data = querystring.stringify(form) || json;
    return api
      .post(url, data, {
        params: querystring.stringify(form),
        baseURL: BASE_URL,
        ...reqConfig,
      })
      .then(async(response) => Promise.resolve(response.data))
      .catch((err) => {
        return Promise.reject(err)
      });
  },

  /**
   * Send request with Content-Type multipart/form
   * used to upload file
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} data
   */
  postData: async (url, data = {}, customConfig = {}) => {
    api.defaults.headers["Content-Type"] = "multipart/form-data";
    api.defaults.timeout = 30000;
    const formData = new FormData();
    const keys = Object.keys(data);
    keys.forEach((key) => {
      data[key] instanceof File
        ? formData.append(key, data[key], data[key].name)
        : formData.append(key, data[key]);
    });
    return api
      .post(url, formData, {
        ...customConfig,
      })
      .then((response) => Promise.resolve(response.data))
      .catch((err) => Promise.reject(err));
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   */
  delete: async (url, json = {}) => {
    const data = json;
    return api
      .delete(url, {
        data,
        baseURL: BASE_URL,
      })
      .then((response) => Promise.resolve(response.data))
      .catch((err) => Promise.reject(err));
  },
};
